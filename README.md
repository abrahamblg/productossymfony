 PRUEBA TÉCNICA BACKEND
========================

¿ Que se ha desarrollado?
--------------
Imaginemos un escenario en el que tenemos un e-commerce y debemos desarrollar unaAPI REST para obtener los productos que serviremos a una app móvil. El producto, secompone de campos básicos como el nombre, una descripción, el precio, una imágen.Además este producto puede pertenecer a una taxonomía (Electrónica, Libros, Moda,...)
 
Para ello, es necesario contar con los siguientes endpoint:

  * **Listado de productos:** Este endpoint debe incluir filtros como la taxonomía delproducto, un rango de precios, o búsqueda en texto. Además, debe de contener unpaginado de los resultados.

  * **Creación de producto:​** Este endpoint debe permitir añadir productos a la base dedatos. La particularidad de este endpoint es que el precio enviado en el endpoint essin impuestos, y debe calcularse y almacenarse el precio con el impuesto aplicado ysin él.

¿ Donde se encuentran esas funcionalidades?
--------------

Controlador base: src/AppBundle/Controller/DefaultController.php

Entities: src/AppBundle/Entity (producto y taxonomia)

Repositorios:  src/AppBundle/Repository (producto y taxonomia)

¿ Como funcionan los endpoint ?
--------------

Existen dos endpoint: 

  **/obtener/**: Listado de productos, método GET, parámetros:

  * idTaxonomia: El id de la taxonomia asociada
  * desde: Precio desde
  * hasta: Precio hasta
  * busquedaTexto: busqueda con like en la descripcion
  * cantidad: cantidad de elementos por página, para la paginación
  * empieza: elemento por el que se empieza a obtener datos, paginación

  El filtrado se realiza en el repositorio

  **/crear/**: Creación de los productos, método POST, parámetros:
  
  * **nombre**: [obligatorio] nombre del producto
  * **descripcion**: [obligatorio] La descripción del producto
  * **imagen**: Una imagen. Se copia dentro de la carpeta /public/ y se guarda la url (La configuración de donde se guarda está en /app/config/services.yaml , parámetro "ruta_imagenes")
  * **precioSinImpuestos**: [obligatorio] Precio sin impuestos. Los impuestos se calculan y se guardan (21% de impuestos, declarado en el controlador)
  * **idTaxonomia**:  El id de la taxonomía a la que pertenece este producto

  La creacion se centraliza en el Controller. En caso de fallo devuelve un HTTP 500

  ¿ Que test se han desarrollado ?
--------------

* Test básico de obtener productos: La llamada al endpoint de listado de productos da un 200

* Test básico de crear productos: La llamada falla al no pasarle parametros

* Test múltiple: Se crea una taxonomia de test, se crea un producto a traves del endpoint, se hacen varios test con el listado y los filtros, se borran los registros de la BBDD

¿ Que Bundles se han utilizado ?
--------------
* **friendsofsymfony/rest-bundle** Bundle para el uso de Rest. Provee de REST completo o configurable (como hemos usado)
* **jms/serializer-bundle** Bundle para la serialización

Comandos utiles para instalacion/uso de test:
--------------

* **php bin/console doctrine:database:create** Creación de la BBDD
* **php bin/console doctrine:schema:update --force** Actualización de la estructura de la BBDD
* **./vendor/bin/simple-phpunit**: Ejecutar los tests 
