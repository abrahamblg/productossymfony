<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Producto
 *
 * @ORM\Table(name="producto")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductoRepository")
 */
class Producto
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=1024)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="decimal", precision=10, scale=2)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="precio_sin_impuestos", type="decimal", precision=10, scale=2)
     */
    private $precioSinImpuestos;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=2048, nullable=true)
     */
    private $imagen;

    /**
     * @ORM\ManyToOne(targetEntity="Taxonomia", inversedBy="producto")
     * @ORM\JoinColumn(name="taxonomia_id", referencedColumnName="id")
     */
    private $taxonomia;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Producto
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Producto
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set precio
     *
     * @param string $precio
     *
     * @return Producto
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set precio ( Sin impuesto )
     *
     * @param string $precio
     *
     * @return Producto
     */
    public function setPrecioSinImpuestos($precio)
    {
        $this->precioSinImpuestos = $precio;

        return $this;
    }

    /**
     * Get precio ( Sin impuesto )
     *
     * @return string
     */
    public function getPrecioSinImpuestos()
    {
        return $precioSinImpuestos->precio;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return Producto
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }


    public function getTaxonomia()
    {
        return $this->taxonomia;
    }

    public function setTaxonomia($taxonomia)
    {
        $this->taxonomia = $taxonomia;

        return $this;
    }
}

