<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

//
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
//
use AppBundle\Entity\Producto;
use AppBundle\Entity\Taxonomia;


class DefaultController extends AbstractFOSRestController
{
    private $taxRate = 1.21;

    /**
    * Endpoint para obtener los productos
    * Permite paginacion (empieza y cantidad)
    * Filtro por precio (con impuestos), idTaxonomia y busqueda de texto en la descripcion
    *
    * @Route("/obtener/")
    * @Method({"GET"})
    */
    public function indexAction(Request $request){
        $filtros =[
            'idTaxonomia'   => $request->get("idTaxonomia"),
            'desde'         => $request->get("desde"),
            'hasta'         => $request->get("hasta"),
            'busquedaTexto' => $request->get("busquedaTexto"),
            'cantidad'      => $request->get("cantidad"),
            'empieza'       => $request->get("empieza"),
        ];
        $repository = $this->getDoctrine()->getRepository(Producto::class);
        $product = $repository->getByFilters($filtros);

        $view = $this->view($product, 200);
        return $this->handleView($view);

    }

    /**
    * Metodo que permite la creacion de un producto
    * Requiere nombre, descripcion y precio
    * Calcula 
    * @Route("/crear/")
    * @Method({"POST"})
    */
    public function creacionAction(Request $request){
        $view = $this->view(['ok'], 200);
        $nombre             = $request->get("nombre");
        $descripcion        = $request->get("descripcion");
        $precioSinImpuestos = $request->get("precioSinImpuestos");
        $idTaxonomia        = $request->get("idTaxonomia");
        $precio             = $precioSinImpuestos * $this->taxRate;
        if($precioSinImpuestos && $descripcion && $nombre){
            
            try{
                $entityManager = $this->getDoctrine()->getManager();
                $producto = new Producto();
                $producto->setNombre($nombre);
                $producto->setDescripcion($descripcion);
                $producto->setPrecio($precio);
                $producto->setPrecioSinImpuestos($precioSinImpuestos);

                $file = $request->files->get('imagen');
                if($file){
                    $fileName = md5(uniqid()).'.'.$file->guessExtension();
    
                    try {
                        $file->move(
                            $this->getParameter('ruta_imagenes'),
                            $fileName
                        );
                    } catch (FileException $e) {
                        $view = $this->view(['error1'], 200);
                    }
                    $producto->setImagen($fileName);   
                }
                
                //Relacion
                $repository = $this->getDoctrine()->getRepository(Taxonomia::class);
                $producto->setTaxonomia($repository->find($idTaxonomia));
                $entityManager->persist($producto);
                $entityManager->flush();
            }
            catch(\Exception $e){
                $view = $this->view(['error'], 500);
            }
        }
        else{
            $view = $this->view(['error'], 500);
        }
        return $this->handleView($view);
    }

}
