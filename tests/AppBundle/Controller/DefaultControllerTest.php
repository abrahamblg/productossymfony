<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

use AppBundle\Entity\Taxonomia;

use AppBundle\Entity\Producto;


class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/obtener/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
    * Si vamos a crear sin todos los parametros, falla con un 500
    */
    public function testPost()
    {
        $client = static::createClient();

        $crawler = $client->request('POST', '/crear/');

        $this->assertEquals(500, $client->getResponse()->getStatusCode());
    }

    /**
    * Test completo:
    * Creamos una categoria de test en la BBDD
    * Llamamos al post para crear un post
    * Lo verificamos por el get (Esta sin filtro, con filtros, y ponemos filtros para que no salga)
    * Borramos taxonomia y post
    */
    public function testPostCrear()
    {



        $client = static::createClient();

        $entityManager =  $client->getContainer()->get('doctrine.orm.entity_manager');
        //getRepository(User::class)->findAll();

        $taxonomia = new Taxonomia();
        $taxonomia->setNombre("Test");
        $entityManager->persist($taxonomia);
        $entityManager->flush();
        $idTaxonomia = $taxonomia->getId();
        //

        $crawler = $client->request('POST',
         	'/crear/',
         	[	"idTaxonomia"=>$taxonomia->getId(),
         		"nombre"=>"Nombre de Test",
         		"descripcion"=>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vehicula risus turpis, et viverra enim dictum sit amet. Sed leo mi, volutpat eget auctor at, molestie vel sapien. In vitae vestibulum enim, vel tincidunt sem.",
         		"precioSinImpuestos"=> 12.34,
         	]
         	);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());


        //Sin parámetros la llamada get obtiene el registo
        $crawler = $client->request('GET', '/obtener/');
        	

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Lorem ipsum dolor sit amet', $client->getResponse()->getContent());

        //Si en el filtro ponemos que la taxonomia es la propia, obtiene el registro
        $crawler = $client->request('GET', '/obtener/', [ "idTaxonomia"=>$idTaxonomia]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Lorem ipsum dolor sit amet', $client->getResponse()->getContent());

        //Si en el filtro ponemos que el desde y hasta del precio podemos hacer que no salga
        $crawler = $client->request('GET', '/obtener/', [ "desde"=>20,"hasta"=>30]);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotContains('Lorem ipsum dolor sit amet', $client->getResponse()->getContent());

        //Borrar el post que hemos hecho
        $post = $entityManager->getRepository(Producto::class)->findByTaxonomia($taxonomia);
        foreach ($post as $thepost) {
        	$entityManager->remove($thepost);
        	$entityManager->flush();
        }

        $tax = $entityManager->getRepository(Taxonomia::class)->findOneById($idTaxonomia);
        //Borrar la categoria de prueba
        $entityManager->remove($tax);
		$entityManager->flush();
    }
}
